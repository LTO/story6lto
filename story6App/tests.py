from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import index, tambahStatus
from .forms import formInput as form
from .models import Status


# Create your tests here.

class story6UnitTest(TestCase):

    def test_ada_landingpage(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_apakah_menggunakan_fungsi_index(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_apakah_menggunakan_fungsi_tambahStatus(self):
        found = resolve('/tambahStatus/')
        self.assertEqual(found.func, tambahStatus)

    def test_apakah_ada_tulisan_halo_apa_kabar(self):
        response = Client().get('')
        self.assertContains(response, 'Halo, apa kabar?')
        self.assertEqual(response.status_code, 200)

    def test_apakah_label_input_status(self):
        response = Client().get('')
        self.assertContains(response, 'Input status')
        self.assertEqual(response.status_code, 200)

    def test_apakah_ada_form_input_status(self):
        response = Client().get('')
        self.assertContains(response, "</form>")
        self.assertEqual(response.status_code, 200)

    def test_apakah_ada_button_submit(self):
        response = Client().get('')
        self.assertContains(response, "</button>")
        self.assertEqual(response.status_code, 200)

    def test_return_string_model(self):
        objTest = Status(status = "testok")
        objTest2 = "testok"
        self.assertEqual(str(objTest), objTest2)

    def test_post_berhasil(self):
        response = self.client.post('/tambahStatus/',data={'status' : 'test'})
        counting_all_available_statusnow = Status.objects.all().count()
        self.assertEqual(counting_all_available_statusnow, 1)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/')
        new_response = self.client.get('/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('test', html_response)

class story6FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(story6FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(story6FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        form = selenium.find_element_by_id('status')

        submit = selenium.find_element_by_id('submit')

        form.send_keys('senang sekali amat bahagia')

        submit.click()


