from django import forms


class formInput(forms.Form):
    status = forms.CharField(max_length=300, widget = forms.TextInput(
        attrs={'class' : 'form-control', 'placeholder' : 'Status', 'id' : 'status'}))
