from django.shortcuts import render, redirect

from django.views.decorators.http import require_POST

from .forms import formInput
from .models import Status


# Create your views here.

def index(request):
    list_status = Status.objects.order_by('waktuInput')
    form = formInput()
    context = {'list_status' : list_status, 'form' : form}
    return render(request, 'landingPage.html', context)

@require_POST
def tambahStatus(request):
    form = formInput(request.POST)

    statusBaru = Status(status = request.POST['status'])

    # statusBaru = Status(statusMasuk = request.POST['inputStatus'])
    statusBaru.save()
    
    return redirect('story6App:index')

