from django.contrib import admin
from django.urls import path
from . import views

app_name = 'story6App'

urlpatterns = [
    path('', views.index, name='index'),
    path('tambahStatus/', views.tambahStatus, name='tambahStatus'),
]
